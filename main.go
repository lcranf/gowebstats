package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	c "github.com/olebedev/config"
)

var (
	config = loadConfig()
)

func main() {

	if config == nil {
		log.Fatalln("Failed to load config file")
	}

	http.HandleFunc("/", defaultHandler)
	fmt.Printf("Starting server on port %v", ":8080")

	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatalf("\nError starting server %v", err)
	}
}

func defaultHandler(w http.ResponseWriter, r *http.Request) {

	webSites := config.UList("SitesToVisit")
	numOfSites := len(webSites)

	if numOfSites == 0 {
		handleError(errors.New("No sites configured to visit.  Please update stats.config"), w)
		return
	}

	channels := make(chan *WebSiteStat, numOfSites)

	var webStats []*WebSiteStat

	for _, site := range webSites {

		fmt.Printf("\nSite to gather stats for: %s", site)

		//let's spawn a go routine for each stats query
		go func(webSiteName string) {

			webSite := WebSiteStat{Name: webSiteName}

			//let's write to the channel to signal that this query is complete
			channels <- webSite.GetStats()
		}(site.(string))
	}

	for i := 0; i < numOfSites; i++ {

		//now let's wait for each routine to finish and gather the stats
		webStats = append(webStats, <-channels)
	}

	//now spit out the stats to the client as json
	json.NewEncoder(w).Encode(webStats)
}

func handleError(err error, w http.ResponseWriter) bool {

	if err == nil {
		return false
	}

	http.Error(w, err.Error(), http.StatusInternalServerError)

	return true

}

func loadConfig() *c.Config {

	file, err := ioutil.ReadFile("stats.config")

	failOnError(err, "Failed to load config file")

	configuration, err := c.ParseJson(string(file))

	failOnError(err, "Error parsing json in config fig")

	return configuration
}
