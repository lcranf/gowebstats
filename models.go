package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/pivotal-golang/bytefmt"
)

// WebSiteStat ...
type WebSiteStat struct {
	Name       string
	SizeOfPage string
	LoadTime   string
	Error      string
}

// GetStats ...
func (ws *WebSiteStat) GetStats() *WebSiteStat {

	startTime := time.Now()
	resp, err := http.Get(fmt.Sprintf("http://%s", ws.Name))

	if err != nil {
		ws.Error = err.Error()
		return ws
	}

	defer resp.Body.Close()

	sizeOfPage, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		ws.Error = err.Error()
		return ws
	}

	ws.SizeOfPage = bytefmt.ByteSize(uint64(len(sizeOfPage)))
	ws.LoadTime = fmt.Sprintf("%.2f sec(s)", time.Now().Sub(startTime).Seconds())

	return ws
}
